//
//  ChatServiceManager.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 13/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol ChatServiceManagerDelegate {
    
    func connectedDevicesChanged(manager : ChatServiceManager, connectedDevices: [String])
    func receivedMessage(manager : ChatServiceManager, userId: String, message: String)
    func receivedPublicKey(manager : ChatServiceManager, publicKeyFriend: String)
    func callPopUp(manager: ChatServiceManager, idOtherUser: String)
    func receivedResponse(manager: ChatServiceManager, response: String, fromPeer: String, type : Int)
}


class ChatData {
    
    struct DataMSG: Codable {
        var idUser: String
        var msg: String
        var publicKey: String
        var type: Int   //0: Message, 1: PublicKey, 2: PopUp, 3: Accept or discard invitation, 4: Start private chat.
    }
    
    var structOfData : DataMSG
    
    init(idUser: String, msg: String, publicKey: String, type: Int) {
        structOfData = DataMSG(idUser: idUser, msg: msg, publicKey: publicKey, type: type)
    }
}



class ChatServiceManager : NSObject {
    // Service type must be a unique string, at most 15 characters long
    // and can contain only ASCII lowercase letters, numbers and hyphens.
    private let ChatServiceType = "inftel"
    
    let myPeerId = MCPeerID(displayName: UIDevice.current.name)
    
    private let serviceAdvertiser : MCNearbyServiceAdvertiser
    private let serviceBrowser : MCNearbyServiceBrowser
    
    var delegate : ChatServiceManagerDelegate?
    
    lazy var session : MCSession = {
        let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        return session
    }()
    
    override init() {
        let security = Security()
        
        let key: [String: String] = ["publicKey" : security.keysGenerator()]
        
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: key, serviceType: ChatServiceType)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: ChatServiceType)
        
        super.init()
        self.serviceAdvertiser.delegate = self
        self.serviceAdvertiser.startAdvertisingPeer()
        
        self.serviceBrowser.delegate = self
        self.serviceBrowser.startBrowsingForPeers()
    }
    
    //"Send" function for sending data to someone in particular.
    func send(chatData : ChatData, toPeer: String) {

        if session.connectedPeers.count > 0 {
            var peerId : MCPeerID
            var arrayPears = [MCPeerID]()
            
            for peer in session.connectedPeers {
                if peer.displayName == toPeer {
                    peerId = peer
                    arrayPears.append(peerId)
                }
            }
            
            do {
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(chatData.structOfData)
                try self.session.send(jsonData, toPeers: arrayPears, with: .reliable)
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)")
            }
        }
        
    }
    
    //"Send" function for sending chat data.
    func send(chatData : ChatData) {
        
        if session.connectedPeers.count > 0 {
            do {
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(chatData.structOfData)
                try self.session.send(jsonData, toPeers: session.connectedPeers, with: .reliable)
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)")
            }
        }
        
    }
    
    //"Send" function for sending data to someone in special.
    func sendToSomeone(chatData : ChatData, fromPeer : String) {
        
        if session.connectedPeers.count > 0 {
            do {
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(chatData.structOfData)
                try self.session.send(jsonData, toPeers: session.connectedPeers, with: .reliable)
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)")
            }
        }
        
    }
    
    
    deinit {
        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
    }
    
}

extension ChatServiceManager : MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        NSLog("%@", "didNotStartAdvertisingPeer: \(error)")
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        NSLog("%@", "didReceiveInvitationFromPeer \(peerID)")
        invitationHandler(true, self.session)
    }
    
}

extension ChatServiceManager : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        NSLog("%@", "didNotStartBrowsingForPeers: \(error)")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        //It is necessary to remove special characters to form the tag.
        let peerIDWithoutSpaces = peerID.displayName.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        let peerIDWithoutHyphen = peerIDWithoutSpaces.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil).folding(options: .diacriticInsensitive, locale: NSLocale.current)
        
        var tag : String = "com.chatios.keys.publickey."
        tag = tag + peerIDWithoutHyphen
        
        var error: Unmanaged<CFError>?
        let options: [String: Any] = [kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                      kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
                                      kSecAttrKeySizeInBits as String: 256]
        
        guard let key = SecKeyCreateWithData(info?.values.first?.dataFromHexadecimalString() as! CFData, options as CFDictionary, &error) else{
                fatalError("Error saving public key from found peer.")
        }
        
        let addquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag.data(using: .utf8)!,
                                       kSecValueRef as String: key]
        
        //Saving the public key from found peer.
        SecItemAdd(addquery as CFDictionary, nil)
        
        browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 10)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        NSLog("%@", "lostPeer: \(peerID)")
    }
    
}

extension ChatServiceManager : MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        NSLog("%@", "peer \(peerID) didChangeState: \(state)")
        self.delegate?.connectedDevicesChanged(manager: self, connectedDevices:
            session.connectedPeers.map{$0.displayName})
    }
    
    func session(_ session: MCSession, didReceive jsonData: Data, fromPeer peerID: MCPeerID) {
        NSLog("%@", "didReceiveData: \(jsonData)")
        
        let jsonDecoder = JSONDecoder()
        
        do{
            let chatData = try jsonDecoder.decode(ChatData.DataMSG.self, from: jsonData)
            NSLog("%@", "didReceiveData: \(chatData.msg)")
            if(chatData.type == 0){ //Receiving a message.
                self.delegate?.receivedMessage(manager: self, userId: chatData.idUser, message: chatData.msg)
            }else if(chatData.type == 1){ //Receiveng a public key.
                self.delegate?.receivedPublicKey(manager: self, publicKeyFriend: chatData.publicKey)
            }else if(chatData.type == 2){ //Call Pop Up.
                self.delegate?.callPopUp(manager: self, idOtherUser: chatData.idUser)
            }else if(chatData.type == 3){ //Accept or discard invitation.
                self.delegate?.receivedResponse(manager: self, response: chatData.msg, fromPeer: chatData.idUser, type : chatData.type)
            }else if(chatData.type == 4){ //Start private chat.
                self.delegate?.receivedResponse(manager: self, response: chatData.msg, fromPeer: chatData.idUser, type : chatData.type)
            }
        } catch{}
    }
    
 
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        NSLog("%@", "didReceiveStream")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        NSLog("%@", "didStartReceivingResourceWithName")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        NSLog("%@", "didFinishReceivingResourceWithName")
    }
    
}

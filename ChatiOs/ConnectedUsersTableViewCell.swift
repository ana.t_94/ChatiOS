//
//  ConnectedUsersTableViewCell.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 15/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import UIKit

class ConnectedUsersTableViewCell: UITableViewCell {

    //MARK: Properties
    
    @IBOutlet weak var userLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

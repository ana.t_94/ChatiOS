//
//  Security.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 14/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import Foundation
import UIKit

class Security {
    
    let ccAvailable : Bool = CC.available()
    let service : String = "inftel"
    
    func keysGenerator() -> String{
        
        var item: CFTypeRef?
        var privateKey : SecKey
        let tag = "com.chatios.keys.mykey".data(using: .utf8)!
        var error: Unmanaged<CFError>?
        
        //Creating the keys (public and private).
        let attributes: [String: Any] = [kSecAttrKeySizeInBits as String: 256,
                                         kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                         kSecPrivateKeyAttrs as String: [kSecAttrIsPermanent as String: true,
                                                                         kSecAttrApplicationTag as String: tag]
                                        ]
        
        let query : [String : Any] = [kSecClass as String: kSecClassKey,
                                      kSecAttrApplicationTag as String: tag,
                                      kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                      kSecReturnRef as String: true]
        
        let getStatus = SecItemCopyMatching(query as CFDictionary, &item)
        
        if getStatus != errSecSuccess {
            privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error)!
            if error != nil {
                fatalError("Error creating the private key.")
            }
        }else{
            privateKey = item as! SecKey
        }
        
        let publicKey = SecKeyCopyPublicKey(privateKey)
        
        guard let raw = SecKeyCopyExternalRepresentation(publicKey as! SecKey, &error) else {
            fatalError("Error - raw")
        }
        
        return (raw as Data).hexadecimalString()
    }
    
    
    func getPrivateKey() -> SecKey{
        var privateKey : SecKey
        let tag = "com.chatios.keys.mykey".data(using: .utf8)!
        var error: Unmanaged<CFError>?
        var item: CFTypeRef?
        
        let attributes: [String: Any] = [kSecAttrKeySizeInBits as String: 256,
                                         kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                         kSecPrivateKeyAttrs as String: [kSecAttrIsPermanent as String: true,
                                                                         kSecAttrApplicationTag as String: tag]
        ]
        
        let query : [String : Any] = [kSecClass as String: kSecClassKey,
                                      kSecAttrApplicationTag as String: tag,
                                      kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                      kSecReturnRef as String: true]
        
        let getStatus = SecItemCopyMatching(query as CFDictionary, &item)
        
        if getStatus != errSecSuccess {
            privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error)!
            if error != nil {
                fatalError("Error creating the private key.")
            }
        }else{
            privateKey = item as! SecKey
        }
        
        return privateKey
    }
    
    func getPeerPublicKey(peerID : String) -> SecKey{
        let tag = ("com.chatios.keys.publickey." + peerID).data(using: .utf8)!
        var resultData: SecKey? = nil
        var result: AnyObject?
        
        let query: Dictionary<String, AnyObject> = [String(kSecAttrKeyType): kSecAttrKeyTypeEC,
                                                    String(kSecAttrKeySizeInBits): 256 as AnyObject,
                                                    String(kSecClass): kSecClassKey,
                                                    String(kSecAttrApplicationTag): tag as AnyObject,
                                                    kSecReturnRef as String : kCFBooleanTrue ]
      
        let status = SecItemCopyMatching(query as CFDictionary, &result)
        
        if status == errSecSuccess {
            resultData = result as! SecKey
            return resultData!
        } else {
            return resultData!
        }
    }
    
    func sharedKeys(friendPublicKey : SecKey, fromPeer : String) -> String{
        
        let tag = "com.chatios.keys.mykey".data(using: .utf8)!
        var error: Unmanaged<CFError>?
        
        let attributes : [String : Any] = [kSecAttrKeySizeInBits as String: 256,
                                           kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                           kSecPrivateKeyAttrs as String: [kSecAttrApplicationTag as String: tag]
                                        ]
        
        guard let shared = SecKeyCopyKeyExchangeResult(getPrivateKey(), SecKeyAlgorithm.ecdhKeyExchangeStandardX963SHA256, friendPublicKey, attributes as CFDictionary, &error)
            else {
                fatalError("Error creating the shared key.")
        }
        
        //Store the shared key.
        KeychainService.savePassword(service: service, account: "com.chatios.keys.sharedkey." + fromPeer, data: (shared as Data).hexadecimalString())
        
        return (shared as Data).hexadecimalString()
    }
}

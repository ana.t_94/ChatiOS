//
//  ConnectedUsersTableViewController.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 15/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import UIKit
import Foundation
import MultipeerConnectivity

class ConnectedUsersTableViewController: UITableViewController {

    var foundPeers = [String]()
    var myPeerId : String = ""
    var chatService : ChatServiceManager? = nil
    var viewController : ViewController? = nil
    var friendID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "People around you"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return foundPeers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ConnectedUsersTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ConnectedUsersTableViewCell else {
            fatalError("The dequeued cell is not an instance of ConnectedUsersTableViewCell")
        }
        
        let peer = foundPeers[indexPath.row]
        cell.userLabel.text = peer
        
        let peerIDWithoutSpaces = peer.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        let peerIDWithoutHyphen = peerIDWithoutSpaces.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil).folding(options: .diacriticInsensitive, locale: NSLocale.current)
        
        if KeychainService.loadPassword(service: "inftel", account: "com.chatios.keys.sharedkey." + peerIDWithoutHyphen) == "" {
            cell.backgroundColor = UIColor.red
        }else{
            cell.backgroundColor = UIColor.green
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let peer : String = foundPeers[(indexPath.row)]
        friendID = peer
        showPopUpStartChat(toPeer: peer)
    }
    
    
    //MARK: Segue function.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "privateChatView" {
            guard let navViewController = segue.destination as? PrivateChatController else {
                fatalError("Unexpected Error \(segue.destination)")
            }
            navViewController.chatService = self.chatService
            navViewController.myPeerId = (self.chatService?.myPeerId.displayName)!
            navViewController.peerFriend = self.friendID
        }
    }
    
    
    //MARK: PopUps.
    //Show a PopUp for starting a new chat.
    func showPopUpStartChat(toPeer : String){
        let chatData = ChatData(idUser: (chatService?.myPeerId.displayName)!, msg: "", publicKey: "", type: 2)
        let alert = UIAlertController(title: "Start Chat", message: "Do you want to start a private chat with \(toPeer)?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.chatService?.send(chatData : chatData, toPeer: toPeer)
            self.viewController?.showPopUpWaitingResponse(fromPeer: toPeer)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel))
        
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

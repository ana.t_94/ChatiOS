//
//  ViewController.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 13/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import UIKit
import Foundation
import MultipeerConnectivity


class ViewController: UIViewController, UITextFieldDelegate {

    let chatService = ChatServiceManager()
    var keyFriend : String = ""
    var devices = [String]()
    var myResponse : String = ""
    var peerFriend : String = ""
    @IBOutlet weak var connectionsLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var scrollScreen: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatService.delegate = self
        chatTextView.backgroundColor = UIColor.clear
        self.title = "CHATiOS"
        messageTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Keyboard functions.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    @IBAction func sendButton() {
        let message : String = self.messageTextField.text!
        
        if message != "" {
            let chat : String = self.chatTextView.text! + chatService.myPeerId.displayName + " said: " + message + "\n"
            self.chatTextView.text = chat
            
            let chatData = ChatData(idUser: chatService.myPeerId.displayName, msg: message, publicKey: "", type: 0)

            chatService.send(chatData: chatData)
            
            self.messageTextField.text = ""
            
            let range = NSMakeRange(chatTextView.text.characters.count - 1, 0)
            chatTextView.scrollRangeToVisible(range)
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollScreen.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollScreen.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        messageTextField.resignFirstResponder()
        return true
    }
    
    
    //MARK: Segue function.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "connectedUsersSegue" {
            guard let navViewController = segue.destination as? ConnectedUsersTableViewController else {
                fatalError("Unexpected Error \(segue.destination)")
            }
            navViewController.foundPeers = devices
            navViewController.chatService = self.chatService
            navViewController.viewController = self
        }else if segue.identifier == "publicToPrivate" {
            guard let navViewController2 = segue.destination as? PrivateChatController else {
                fatalError("Unexpected Error \(segue.destination)")
            }
            navViewController2.chatService = self.chatService
            navViewController2.myPeerId = self.chatService.myPeerId.displayName
            navViewController2.peerFriend = self.peerFriend
        }
    }
    
    
    //MARK: PopUps.
    //Show a PopUp with a new chat alert.
    func showPopUpStartChat(fromPeer: String){
        let alert = UIAlertController(title: "New chat alert", message: "\(fromPeer) wants to chat with you. Do you agree?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //The other user is notified that the invitation is accepted.
            let chatData = ChatData(idUser: self.chatService.myPeerId.displayName, msg: "Yes", publicKey: "", type: 3)
            self.chatService.send(chatData : chatData, toPeer: fromPeer)
            
            //Diffie-Hellman.
            let security = Security()
            let peerIDWithoutSpaces = fromPeer.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
            let peerIDWithoutHyphen = peerIDWithoutSpaces.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil).folding(options: .diacriticInsensitive, locale: NSLocale.current)
            let friendPublicKey = security.getPeerPublicKey(peerID: peerIDWithoutHyphen)
            let sharedKey : String = security.sharedKeys(friendPublicKey: friendPublicKey, fromPeer: peerIDWithoutHyphen)
            
            //Show the PopUp with the shared key.
            self.showPopUpSharedKeys(sharedKey: sharedKey, fromPeer: fromPeer)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            //The other user is notified that the invitation is canceled.
            let chatData = ChatData(idUser: self.chatService.myPeerId.displayName, msg: "No", publicKey: "", type: 3)
            self.chatService.send(chatData : chatData, toPeer: fromPeer)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Show a PopUp with the shared key.
    func showPopUpSharedKeys(sharedKey : String, fromPeer: String){
        let alert = UIAlertController(title: "Shared Keys", message: "This is the shared key: \(sharedKey)\n Are both keys equals?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //The "Yes" is sent to the other user, and if the other one also presses "Yes", the private chat starts.
            self.myResponse = "Yes"
            let chatData = ChatData(idUser: self.chatService.myPeerId.displayName, msg: "Yes", publicKey: "", type: 4)
            self.chatService.send(chatData : chatData, toPeer: fromPeer)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            //The "No" is sent to the other user.
            self.myResponse = "No"
            let chatData = ChatData(idUser: self.chatService.myPeerId.displayName, msg: "No", publicKey: "", type: 4)
            self.chatService.send(chatData : chatData, toPeer: fromPeer)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Show a PopUp for waiting response.
    func showPopUpWaitingResponse(fromPeer: String){
        let alert = UIAlertController(title: "Waiting response", message: "An invitation has send to: \(fromPeer)\n Please, wait...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    //Shows a PopUp for cancelling the chat invitation.
    func cancelChat(fromPeer : String){
        self.myResponse = ""
        //The previously stored shared key is deleted.
        KeychainService.removePassword(service: "inftel", account: "com.chatios.keys.sharedkey." + fromPeer)
        //A PopUp appears notifying the cancellation of the chat.
        self.dismiss(animated: false, completion: nil)
        let alert = UIAlertController(title: "Shared key conflict", message: "Shared keys don't match. Try again.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController : ChatServiceManagerDelegate {
    func receivedResponse(manager: ChatServiceManager, response: String, fromPeer: String, type: Int) {
        let peerIDWithoutSpaces = fromPeer.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        let peerIDWithoutHyphen = peerIDWithoutSpaces.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil).folding(options: .diacriticInsensitive, locale: NSLocale.current)
        peerFriend = fromPeer
        
        if type == 3{ //Acceptance of private chat.
            if response == "Yes" {
                //The shared key is created.
                let security = Security()
                let friendPublicKey = security.getPeerPublicKey(peerID: peerIDWithoutHyphen)
                let sharedKey : String = security.sharedKeys(friendPublicKey: friendPublicKey, fromPeer: peerIDWithoutHyphen)
                
                //Show the PopUp with the shared key.
                self.dismiss(animated: false, completion: nil)
                self.showPopUpSharedKeys(sharedKey: sharedKey, fromPeer: self.chatService.myPeerId.displayName)
                
            }else{
                //A new PopUp is shown communicating that the private chat is canceled.
                self.dismiss(animated: false, completion: nil)
                let alert = UIAlertController(title: "Ups...", message: "It seems that \(fromPeer) doesn't want to talk with you...", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay...", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            }
        }else if type == 4{ //Private chat initiation
            if self.myResponse == "Yes"{ //Both keys match.
                if response == "Yes"{
                    //Go to the private chat view.
                    self.performSegue(withIdentifier: "publicToPrivate", sender: self)
                }else if response == "No"{
                    cancelChat(fromPeer : peerIDWithoutHyphen)
                }
            }else if self.myResponse == "No" { //Both keys do not match.
                cancelChat(fromPeer : peerIDWithoutHyphen)
            }
        }
    
    }
    
    func callPopUp(manager: ChatServiceManager, idOtherUser: String) {
        showPopUpStartChat(fromPeer: idOtherUser)
    }
    
    func receivedPublicKey(manager: ChatServiceManager, publicKeyFriend: String) {
        OperationQueue.main.addOperation {
            self.keyFriend = publicKeyFriend
        }
    }
    
    
    func connectedDevicesChanged(manager: ChatServiceManager, connectedDevices: [String]) {
        OperationQueue.main.addOperation {
            //self.connectionsLabel.text = "Connections: \(connectedDevices)"
            self.devices = connectedDevices
        }
    }
    
    func receivedMessage(manager: ChatServiceManager, userId: String, message: String) {
        OperationQueue.main.addOperation {
            let chat : String = self.chatTextView.text! + userId + " said: " + message + "\n"
            self.chatTextView.text = chat
            
            let range = NSMakeRange(self.chatTextView.text.characters.count - 1, 0)
            self.chatTextView.scrollRangeToVisible(range)
        }
    }
}

//
//  PrivateChatController.swift
//  ChatiOs
//
//  Created by INFTEL 01 on 16/2/18.
//  Copyright © 2018 INFTEL 01. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity


class PrivateChatController: UIViewController, UITextFieldDelegate {
    
    var chatService : ChatServiceManager? = nil
    var myPeerId : String = ""
    var peerFriend : String = ""
    
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var scrollScreen: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatTextView.backgroundColor = UIColor.clear
        self.title = "Private chat"
    }

    
    @IBAction func sendButton() {
        let message : String = self.messageTextField.text!
        
        if message != "" {
            let chat : String = self.chatTextView.text! + (chatService?.myPeerId.displayName)! + " said: " + message + "\n"
            self.chatTextView.text = chat
            
            var cipher_gcm : Data
            
            let data = message.data(using: .utf8)
            let aditional_data = ((chatService?.myPeerId.displayName)! + peerFriend).data(using: .utf8)
            let sharedKey = KeychainService.loadPassword(service: "inftel", account: "com.chatios.keys.sharedkey." + peerFriend)
            let aesKey = (sharedKey)?.data(using: .utf8)
            let iv = "privateChanel".data(using: .utf8)
            
            do{
                cipher_gcm = try CC.cryptAuth(.encrypt, blockMode: .gcm, algorithm: .aes, data: data!, aData: aditional_data!, key: aesKey!, iv: iv!, tagLength: 16)
            }catch{
                fatalError("Impossible to cipher the message.")
            }
            
            
            let chatData = ChatData(idUser: (chatService?.myPeerId.displayName)!, msg: cipher_gcm.hexadecimalString(), publicKey: "", type: 0)
            
            chatService?.send(chatData : chatData, toPeer: peerFriend)
            
            self.messageTextField.text = ""
            
            let range = NSMakeRange(chatTextView.text.characters.count - 1, 0)
            chatTextView.scrollRangeToVisible(range)
        }
    }
    
    
    //MARK: Keyboard functions.
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollScreen.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollScreen.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        messageTextField.resignFirstResponder()
        return true
    }
    
    
}

extension PrivateChatController : ChatServiceManagerDelegate {
    func connectedDevicesChanged(manager: ChatServiceManager, connectedDevices: [String]) {
        //
    }
    
    func receivedMessage(manager: ChatServiceManager, userId: String, message: String) {
        OperationQueue.main.addOperation {
            
            var decipher_gcm : Data
            
            let aditional_data = ((self.chatService?.myPeerId.displayName)! + self.peerFriend).data(using: .utf8)
            let sharedKey = KeychainService.loadPassword(service: "inftel", account: "com.chatios.keys.sharedkey." + self.peerFriend)
            let aesKey = (sharedKey)?.data(using: .utf8)
            let iv = "privateChanel".data(using: .utf8)
            
            do{
                decipher_gcm = try CC.cryptAuth(.decrypt, blockMode: .gcm, algorithm: .aes, data: message.dataFromHexadecimalString()!, aData: aditional_data!, key: aesKey!, iv: iv!, tagLength: 16)
            }catch{
                fatalError("Impossible to decipher the message.")
            }
            
            let chat : String = self.chatTextView.text! + userId + " said: " + String(data: decipher_gcm, encoding: .utf8)! + "\n"
            self.chatTextView.text = chat
            
            let range = NSMakeRange(self.chatTextView.text.characters.count - 1, 0)
            self.chatTextView.scrollRangeToVisible(range)
        }
    }
    
    func receivedPublicKey(manager: ChatServiceManager, publicKeyFriend: String) {
        //
    }
    
    func callPopUp(manager: ChatServiceManager, idOtherUser: String) {
        //
    }
    
    func receivedResponse(manager: ChatServiceManager, response: String, fromPeer: String, type: Int) {
        //
    }
    
    
}

